# sandbox-registry

This project does not contain any code, it is just intended to be used as a sandbox registry to host container images used for development purposes.

At some point we indeed may need to build and test a custom image in some part of the sylva project while developing a new component, or in order to test some changes in an upstream project. As this kind of images do not deserve a dedicated project with a build pipeline, this project can be used for convenience as a temporary registry for these images.

The registry can be browsed **[here](https://gitlab.com/sylva-projects/sylva-elements/container-images/sandbox-registry/container_registry)**.

In order to use it, just push your image in this registry's project.

## pushing with `docker`

You'll may need to log into the registry first:
```
docker login registry.gitlab.com
# use your gitlab username and as a password a gitlab token with write_registry privileges
```

Then you can tag and push:
```
docker tag dev-image:latest registry.gitlab.com/sylva-projects/sylva-elements/container-images/sandbox-registry/dev-image:tag
docker push registry.gitlab.com/sylva-projects/sylva-elements/container-images/sandbox-registry/dev-image:tag
```

## pushing with `skopeo`

Example with `skopeo` (also requires a `docker login` first):

```
skopeo copy docker://eu.gcr.io/k8s-artifacts-prod/storage-migrator/storage-version-migration-migrator:v0.0.5 docker://registry.gitlab.com/sylva-projects/sylva-elements/container-images/sandbox-registry/storage-version-migration-migrator:v0.0.5
```

